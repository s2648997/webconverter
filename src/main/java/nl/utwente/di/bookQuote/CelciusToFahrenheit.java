package nl.utwente.di.bookQuote;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CelciusToFahrenheit extends HttpServlet {

    private CelciusToFahrenheit converter;
    private static final long serialVersionUID = 1L;

    public double convertToFahrenheit(float celsius){
        double fahrenheit = 0;
        fahrenheit = (celsius*1.8)+32;
        return fahrenheit;
    }
    public void init() throws ServletException {
        converter = new CelciusToFahrenheit();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        String docType = "<!DOCTYPE HTML>\n";
        writer.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE" + "Celcius to Fahrenheit converter" + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + "Celcius to Fahrenheit converter" + "</H1>\n" +
                "  <P>Celsius temperature: " +
                request.getParameter("Celsius") + "\n" +
                "  <P>Fahrenheit temperature: " +
                Double.toString(converter.convertToFahrenheit(Float.parseFloat(request.getParameter("Celsius")))) +
                "</BODY></HTML>");
    }

}
