package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    HashMap<String, Double> map = new HashMap<>();
    public double getBookPrice(String isbn){
        double answer = 0;
        map.put(" 1 ", 10.0);
        map.put(" 2 ", 45.0);
        map.put(" 3 ", 20.0);
        map.put(" 4 ", 35.0);
        map.put(" 5 ", 50.0);
        map.put(" others ", 0.0);
        for(String key: map.keySet()){
            if(key==isbn){
                answer = map.get(key);
                break;
            }
        }
        return answer;

    }
}
